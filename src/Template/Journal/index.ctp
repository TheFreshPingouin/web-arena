<h1>Page de journal</h1>

<table id="table1" class="table table-bordered dataTable no-footer" role="grid" aria-describedby="table1_info" style="width: 100%;" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                Date de l'évènement
            </th>
            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                Description
            </th>
            <th class="sorting" tabindex="0" aria-controls="table1" rowspan="1" colspan="1" style="width: 113px;" aria-label="Name: activate to sort column ascending">
                Coordonnées
            </th>
        </tr>
    </thead>
    <tbody>

    <?php
        foreach($events as $event){
            echo("<tr>");

            echo("<td>"
                .$event['date'] ."</td>");
            echo("<td>"
                .$event['name'] ."</td>");
            echo("<td>"
                . " X=".$event['coordinate_x']." Y=".$event['coordinate_y'] ."</td>");

            echo("</tr>");
        }

    ?>

    </tbody>
</table>