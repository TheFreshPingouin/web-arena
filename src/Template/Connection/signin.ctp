<!-- src/Template/Users/add.ctp -->

<div class="users form">
<?= $this->Form->create('Post', array('action' => 'add')); ?>
    <fieldset>
        <legend><?= __('Ajouter un utilisateur') ?></legend>
        <?= $this->Form->control('email') ?>
        <?= $this->Form->control('password') ?>
    </fieldset>
<?= $this->Form->button(__('Inscription')); ?>
<?= $this->Form->end() ?>
</div>
