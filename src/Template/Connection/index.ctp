<h1>Page de connection</h1>
<?php echo  $this->form->create('FormLogin', array('controller'=>'connection', 'action' => '/login')); ?>
<?= $this->Form->button(('Connexion')); ?>
<?= $this->Form->end() ?>

<?php echo  $this->form->create('FormSignIn', array('controller'=>'connection', 'action' => '/signin')); ?>
<?= $this->Form->button(('Inscription')); ?>
<?= $this->Form->end() ?>

<?php echo  $this->form->create('FormForgottenPwd', array('controller'=>'connection', 'action' => '/pwd')); ?>
<?= $this->Form->button(('Mot de passe oublié')); ?>
<?= $this->Form->end() ?>