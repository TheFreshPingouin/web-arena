
<div class="container">
    <?php if($info['current_health'] <= 0) echo '<div class="alert alert-danger" role="alert">You are dead. Please, create a new character</div>' ?>

  <div class="row">
          <div class="col-4">
        
        <div class="row">
          <?php
             $avatarPath = 'avatar/'.$info['player_id'].'.jpg';
             echo $this->Html->image($avatarPath, ['alt' => 'no avatar chosen yet']);
          ?>
        </div>
        <div class="row">
          <?php
             echo $this->Form->create('formform', ['type' => 'file']);
             echo $this->Form->input('upload', ['type' => 'file']);
             echo $this->Form->button('Update avatar (jpg file only)', ['class' => 'btn btn-lg btn-success btn-block']);
             echo $this->Form->end(); 
          ?>
        </div>
        
    </div>
    <div class="col-8">
        
        <div class="row">
        <h2><?php echo $info['name'] ?></h2>
        </div>
        <div class="row">
          <p>level: <?php echo $info['level'] ?></p>
        </div>
        <div class="row-12">
            <div class="progress">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?php echo $info['xp']-(($info['level']-1)*4) ?>" aria-valuemin="0" aria-valuemax="4" style="width: <?php echo (($info['xp']-(($info['level']-1)*4))/4)*100; ?>%;">
                    XP: <?php echo $info['xp']-(($info['level']-1)*4) ?> / 4
                </div>
            </div>         
        </div>
        <div class="row">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Skill</th>
                <th>points</th>
              </tr>
            </thead>
            <tbody>

              <tr>
                <td><p>Sight <span class="glyphicon glyphicon-envelope"></span> </p></td>
                <td>                
                <?php echo $info['skill_sight'] ?>           
                <?php if($lvl == 1 && $info['current_health'] >= 0) { 
                  echo $this->Form->postButton('upgrade',
                  ['class' => ['btn','btn-success'],
                  'controller' => 'Fighter','action' => 'addpoint'],
                  ['data' => ['upgrade' => 'sight']]); 
                  }
                  ?>                  
                </p></td>
              </tr>
              <tr>
                <td>Strength</td>
                <td><?php echo $info['skill_strength'] ?>
                <?php if($lvl == 1 && $info['current_health'] >= 0) { 
                  echo $this->Form->postButton('upgrade',
                  ['class' => ['btn','btn-success'],
                  'controller' => 'Fighter','action' => 'addpoint'],
                  ['data' => ['upgrade' => 'strength']]); 
                  } 
                  ?>               
                </td>
              </tr>
              <tr>
                <td>health</td>
                <td>                
                <?php echo $info['skill_health'] ?>
                <?php if($lvl == 1 && $info['current_health'] >= 0) { 
                  echo $this->Form->postButton('upgrade',
                  ['class' => ['btn','btn-success'],
                  'controller' => 'Fighter','action' => 'addpoint'],
                  ['data' => ['upgrade' => 'health']]); 
                  } 
                  ?>                
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <div class="row-12">
            <div class="progress" >
                <div class="progress-bar  <?php if($info['current_health'] == 1) echo 'bg-danger' ?> progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="<?php echo $info['skill_health'] ?>" aria-valuemin="0" aria-valuemax="<?php echo $info['skill_health'] ?>" style="width:<?php echo floor(($info['current_health']/$info['skill_health'])*100) ?>%;">
                    HP: <?php echo $info['current_health'] ?>/<?php echo $info['skill_health'] ?>
                </div>
            </div>
        </div>
        <div class="row">
        <p>guild: <?php echo $info['guild_id'] ?></p>
        </div>
        <div class="row">
        <?php 
          
          echo $this->Form->create();
          echo $this->Form->text('fighter name', ['class' => 'users']);
          echo $this->Form->button('create a new fighter', ['type' => 'submit']);
          echo $this->Form->end();
                  
                  ?>
        </div>
    </div>
  </div>
</div>