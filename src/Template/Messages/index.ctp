
<div class="jumbotron jumbotron-fluid">
    <div class="container">
        <h3>Your Messages</h1>

        <div id="mess" class="jumbotron jumbotron-fluid">
            <div class="container">

<?php

foreach ($messages as $key => $value) {
    echo "<p><b>".$value['fighter_id_from'].": </b>".$value['message']."</p>";
}

?>
            </div>
        </div>

        <div id="write" class="jumbotron jumbotron-fluid">
            <div class="container">

                <form>
                    <div class="">
                        <div class="form-row">
                            <input type="text" class="form-control" placeholder="Enter your message here...">
                        </div>
                        <div class="form-row">
                            <label for="tofighter">Choose a fighter :</label>
                            <select id="tofighter" class="custom-select" data-style="btn-warning" title="Choose a fighter to talk to...">
                                <option>Mustard</option>
                                <option disabled>Ketchup</option>
                                <option>Relish</option>
                            </select>
                        </div>
                        <div class="form-row">
                            <a class="btn btn-primary" href="#" role="button">Send message</a>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>

    </div>
</div>
