<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->script('jquery-3.2.1') ?>
    <?= $this->Html->script('popper') ?>
    <?= $this->Html->script('jquery.jqplot.js') ?>
    <?= $this->Html->css('bootstrap.min') ?>
    <?= $this->Html->script('bootstrap.min') ?>
    <?= $this->Html->css('sticky-footer') ?>
    <?= $this->Html->css('jquery.jqplot.css') ?>
    <?= $this->Html->css('jquery.dataTables.min') ?>
    <?= $this->Html->script('jquery.dataTables.min') ?>
    <?= $this->Html->css('webarena') ?>
    <?= $this->Html->script('webarena') ?>
    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body>
    <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <?php echo $this->Html->link(
            'WebArena',
            '/home',
            ['class' => 'navbar-brand']
            ); ?>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarCollapse">
              <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                  <?php echo $this->Html->link(
                    'Home',
                    '/home',
                    ['class' => 'nav-link']
                    ); ?>
                </li>
                <?php if(!($loggedIn)) : ?>
                <li class="nav-item">
                  <?php echo $this->Html->link(
                    'Connection',
                    '/connection',
                    ['class' => 'nav-link']
                    ); ?>
                </li>
                <?php endif; ?>
                <li class="nav-item">
                  <?php echo $this->Html->link(
                    'Fighter',
                    '/fighter',
                    ['class' => 'nav-link']
                    ); ?>
                    <!-- <a class="nav-link disabled" href="#">Fighter</a> -->
                </li>
                <li class="nav-item">
                  <?php echo $this->Html->link(
                    'Vision',
                    '/vision',
                    ['class' => 'nav-link']
                    ); ?>
                </li>
                <li class="nav-item">
                  <?php echo $this->Html->link(
                    'Journal',
                    '/journal',
                    ['class' => 'nav-link']
                    ); ?>
                </li>
                <li class="nav-item">
                  <?php echo $this->Html->link(
                    'Hall of fame',
                    '/Halloffame',
                    ['class' => 'nav-link']
                    ); ?>
                </li>
                <?php if($loggedIn) : ?>
                <li class="nav-item">
                  <?php echo $this->Html->link(
                    'Logout',
                    ['controller' => 'connection', 'action' => 'logout'],
                    ['class' => 'nav-link']
                    ); ?>
                </li>
                <?php endif; ?>

            </ul>
        </div>
    </nav>
</header>

<?= $this->Flash->render() ?>
<div class="container clearfix">
    <?= $this->fetch('content') ?>
</div>

<footer class="footer">
  <div class="container">
    <span class="text-muted">Group Name | CASTEL, KHAMES, MAHL, NEGRIER | Options : B & E | Version log</span>
</div>
</footer>

</body>
</html>