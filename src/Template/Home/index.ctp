<div class="container">
  <div class="row">
    <div class="col-3">
        <h5>Ceci est un slider des profils des fighters existants:</h5>
            <div id="carouselAvatars" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <?php
            $classActive = ' class="active"';
            for($i = 0; $i <= count($info)-1; $i++){

                echo '<li data-target="#carouselAvatars" data-slide-to="'.$i.'"'.$classActive.'></li>';
                $classActive = '';
            }

            ?>
          </ol>
          <div class="carousel-inner" role="listbox">
            <?php 
            //pr($info);

            $activity = ' active';

            foreach($info as $player){



                echo '<div class="carousel-item'.$activity.'">';
                $activity = '';
                $avatarPath = 'avatar/'.$player['id'].'.jpg';
                echo $this->Html->image($avatarPath, ['class' => 'd-block img-fluid', 'alt' => 'no avatar for this player yet']);
                echo '<div class="carousel-caption d-none d-md-block"><h3>'.$player['email'].'</h3></div></div>';

            }
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselAvatars" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselAvatars" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

    </div>
    <div class="col-9">
        <h2>Règles du jeu</h2>
        <b>Les règles sont les suivantes :</b>
        <ul>
            <li>
                Un combattant se trouve dans une arène en damier à une position X,Y. Cette position ne peut
                pas se trouver hors des dimension de l'arène. Un seul combattant par case. Une arène par
                site.
            </li>
            <li>
                Un combattant commence avec les caractéristiques suivantes : vue= 2, force=1, point de
                vie=5 (ces valeurs doivent être paramétrées). Il apparaît à une position libre aléatoire.
            </li>
            <li>
                Constantes paramétrées et valeurs de livraison : largeur (x) de l'arène (15), longueur (y) de
                l'arène (10) (ces valeurs doivent être paramétrées dans un modèle).
            </li>
            <li>
                La caractéristique de vue détermine à quelle distance (de Manhattan = x+y) un combattant
                peut voir. Ainsi seuls les combattants et les éléments du décor à portée sont affichés sur la
                page concernée. 0 correspond à la case courante.
            </li>
            <li>
                La caractéristique de force détermine combien de point de vie perd son adversaire quand le
                combattant réussit son action d'attaque..
            </li>
            <li>
                Lorsque le combattant voit ses points de vie atteindre 0, il est retiré du jeu. Un joueur dont le
                combattant a été retiré du jeu est invité à en recréer un nouveau.
            </li>
            <li>
                Une action d'attaque réussit si une valeur aléatoire entre 1 et 20 (d20) est supérieur à un seuil
                calculé comme suit : 10 + niveau de l'attaqué - niveau de l'attaquant.
            </li>
            <li>
                Progression : à chaque attaque réussie le combattant gagne 1 point d'expérience. Si l'attaque
                tue l'adversaire, le combattant gagne en plus autant de points d'expériences que le niveau de
                l'adversaire vaincu. Tous les 4 points d'expériences, le combattant change de niveau et peut
                choisir d'augmenter une de ses caractéristiques : vue +1 ou force+1 ou point de vie+3. En
                cas de progression, les points de vie maximaux augmentent ET les points de vie courants
                remontent au maximum.
            </li>
            <li>
                En pratique, on incrémentera le niveau que lorsqu'une augmentation sera prise, et on
                utilisera (xp/4) - niveau pour savoir s'il reste des augmentations à prendre. Le niveau
                commence et les points d'expérience commencent à 0.
            </li>
            <li>
                Chaque action provoque la création d'un événement avec une description claire. Par
                exemple : « jonh attaque bill et le touche ».
            </li>
        </ul>
    </div>
  </div>
 </div>