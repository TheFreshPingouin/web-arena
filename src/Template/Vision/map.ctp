
<?php
$min_height = 50;
$playerColor = '#6a6';
$couleurFond = '';
$sourceImage = '';
$description = '';

foreach ($map as $key => $value) {
    $j = $key % $traySize['w'];
    $i = (int) floor($key / $traySize['w']);
    
    if($j == 0){
        echo '<div class="row">';
    }
    
    $couleurFond = '#fff';
    if($i == $playerPosition['coordinate_x'] && $j == $playerPosition['coordinate_y']){
        // The player
        $isPlayer = 'player';
        $couleurFond = $playerColor;
        $sourceImage = '/webArena/img/fighter.jpg';
        $description = 'This is your player';
    }
    else{
        // Visibility zone of the player, he can see it
        $isPlayer = '';
        if($value == '_'){
            $couleurFond = '#ccc';
            $sourceImage = '/webArena/img/grass.jpg';
            $description = 'There is nothing here';
        }
        else{
            if($value == 'E'){
                $couleurFond = '#a66';
                $sourceImage = '/webArena/img/fighter2.jpg';
                $description = 'Careful ! An enemy !!!';
            }
            else{
                // Else he can't see it : war fog
                $couleurFond = '#101';
                $sourceImage = '';
                $description = "You can't see that far";
            }
        }
    }
    
    echo '<div data-toggle="tooltip" data-placement="top" title="'.$description.'" class="col select '.$isPlayer.'" id="col-'. $key .'" '
            . 'style="background-color:' . $couleurFond . '; background-image: url('.$sourceImage.'); min-height:' 
            . $min_height . 'px' . ';">';
    if($value == 'E' || $value == 'O'){
        $found = false;
        foreach ($fighters as $k => $f) {
            if($i == $f['coordinate_x'] && $j == $f['coordinate_y'] && $f['current_health'] > 0){
                $found = true;
                break;
            }
        }
        $name = $f['name'];
        $pv = $f['current_health'];
        $maxpv = $f['skill_health'];
        $level = $f['level'];
        
        echo '<div class="progress"><div class="progress-bar" style="width: '. ($pv/$maxpv)*100 .'%" role="progressbar" aria-valuenow="'. $pv .'" aria-valuemin="0" aria-valuemax="'. $maxpv .'">'.$pv.'/'.$maxpv.'</div></div>';
        echo '<p><b>'. $name .' (lvl '. $level .')</b></p>';
    }
    
    echo '</div>';
    
    if($j == $traySize['h']-1){
        echo '</div>';
    }
}
?>