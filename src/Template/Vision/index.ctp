<div class="row">
    <div class="col-2" id="description">
        <?= $this->Html->image('description_vision.png', ['alt' => 'TIME TO FIGHT']); ?>
    </div>
  <div style="background-color: #200;" class="col-7" id="map"></div>
  <div style="display:none;" id="controls">
        <?php
        $controller = 'vision';
        $action = 'index';

        echo $this->Form->PostButton('Up', ['controller' => $controller,'action' => $action], [
                    'data' => ['dep' => '0'],
                    'id' => 'buttonUp'
                    ]);
        echo $this->Form->PostButton('Left', ['controller' => $controller,'action' => $action], [
                    'data' => ['dep' => '1'],
                    'id' => 'buttonLeft'
                    ]);
        echo $this->Form->PostButton('Right', ['controller' => $controller,'action' => $action], [
                    'data' => ['dep' => '2'],
                    'id' => 'buttonRight'
                    ]);
        echo $this->Form->PostButton('Down', ['controller' => $controller,'action' => $action], [
                    'data' => ['dep' => '3'],
                    'id' => 'buttonDown'
                    ]);
        ?>
  </div>
  <div style="background-color: #300; text-align: center;" class="col-3" id="messenger">
      <!--<b style="color: #ddd; ">Messenger is here</b>-->
  </div>
</div>

<div style="display: none;" class="sticky-button-top-right">
    Nice mode
</div>
