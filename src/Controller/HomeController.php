<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

class HomeController extends AppController {
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow();
    }
    
    public function index() {

        $db = $this->loadModel('Players');
        $info = $db->getInfo();
        $this->set('info',$info);
        
    }
    
}
