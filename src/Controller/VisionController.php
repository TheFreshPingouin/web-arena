<?php

namespace App\Controller;
use App\Controller\AppController;

class VisionController extends AppController {
    
    public function index() {
        //pr("hi");
    }
    
    public function map(){
        
        // Créer un evenement par action dans le journal
        
        // Load models
        $fight = $this->loadModel('Fighters');
        $surround = $this->loadModel('Surroundings');
        $events = $this->loadModel('Events');
        
        // A mettre en données de session
        $newEventName = "";
        $userId = $this->request->session()->read('Auth.User')['id'];
        $id = $fight->getFighterId($userId);
        $isOtherPlayerHere = false;
        $traySize = array(
            'w' => 15,
            'h' => 10
            );
        
        // Si index est appelé en ajax, on fait un truc particulier
        if($this->request->is('post')){
            $sentDataFromForm = $this->request->getData('dep');
            //debug($sentDataFromForm);
            $fighter = $fight->get($id);
            //$fighterIo = $fight->getFighterById($fighter);

            
            if(($sentDataFromForm == 0 || $sentDataFromForm == 1 || $sentDataFromForm == 2 || $sentDataFromForm == 3) && $fighter != null){
                if($fighter['current_health'] > 0){
                    // Si il n'y a rien, il se déplace.
                    if($enemy = $fight->isAnyFighterOn($id, $sentDataFromForm)) {
                        // ATTAK ?
                        $isOtherPlayerHere = true;

                        $action = "";
                        
                        // Auto attack without confirmation
                        if($fight->attack($enemy, $fighter)){
                            // Add to the events table
                            if($enemy['current_health'] == 0)
                            {
                                $action = " killed ";
                            }
                            else{
                                $action = " attacked ";
                            }
                            //$this->Flash->success(__('You got him.'));
                            
                            $newEventName = $fighter['name'] . $action . $enemy['name'] . " !";
                            $events->setNewEvent($newEventName, $fighter);

                            if($enemy['current_health'] == 0){
                                $newEventName = $enemy['name'] . " was slained !";
                                $events->setNewEvent($newEventName, $enemy);
                            }
                            if($fighter['current_health'] == 0){
                                $newEventName = $fighter['name'] . " killed himself ! (trololol)";
                                $events->setNewEvent($newEventName, $fighter);
                            }
                        }
                        else {
                            $newEventName = $fighter['name'] . " missed " . $enemy['name'] . ", try again !!!";
                            $events->setNewEvent($newEventName, $fighter);
                            //$this->Flash->success(__('You missed him.'));
                        }
                    }
                    else {
                        // If no other obstacles, fighter can move
                        $isOtherPlayerHere = false;
                        $success = $fight->updateFighterById($id, $sentDataFromForm, $traySize);
                        $newEventName = $fighter['name'] . " moved !";
                        $events->setNewEvent($newEventName, $fighter);
                        if($success != 0){
                            //$this->Flash->success(__('The position was updated.'));
                        }
                        else{
                            //$this->Flash->error(__('The position was not updated.'));
                        }
                    }
                }
            }
        }
        
        // Get the fighter's information
        $fighterInfo = $fight->getFighterById($id);
        
        if($fighterInfo['current_health'] <= 0){
            echo 'Your fighter is dead';
        }
        // Get all fighters
        $fighters = $fight->getAllFighters();
        //debug($fighters);
        
        // Generate the environment
        $map = $surround->generateSurroundings($traySize, $fighterInfo, $fighters);
        
        // Pass values to the view
        $this->set('map', $map);
        $this->set('traySize', $traySize);
        $this->set('playerPosition', $fighterInfo);
        
        // If you want confirmation to attack, it's here
        $this->set('askFight', $isOtherPlayerHere);
        
        // Give the players' information to the view to get the names and hp of each fighter
        $this->set('fighters', $fighters);
        
    }
}