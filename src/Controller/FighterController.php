<?php
    namespace App\Controller;

    use App\Controller\AppController;

    class FighterController extends AppController
    {
        public function index()
        {

            $playerId = $this->request->session()->read('Auth.User')['id'];
            
            $db = $this->loadModel('Fighters');
            
            
            $data = $this->request->getData();
            if(!empty($data)){

                if (!empty($this->request->data['upload']['name'])) {
                    $file = $this->request->data['upload']; 
                    
                    $ext = substr(strtolower(strrchr($file['name'], '.')), 1); 
                    $arr_ext = array('jpg'); //only jpg are possible for this version
                    
                    if (in_array($ext, $arr_ext)) {
                        move_uploaded_file($file['tmp_name'], WWW_ROOT . '/img/avatar/' . $playerId . '.' . $ext);
                    }
                }

                else{

                    if($data['fighter_name'] ){
        
                        if( preg_match("/[a-z]/i", $data['fighter_name'])){
            
                            $db->newFighter($playerId,$data['fighter_name']);
        
                        }
                        else{
        
                            pr('<div class="alert alert-warning" role="warning">Your fighter name needs to contain letters</div>');
                        }
                    }
                    else{
                        
                             pr('<div class="alert alert-warning" role="warning">choose a name for your fighter</div>');
                    }
                }

                

            }

            
            

            $fighterId = $db->getFighterId($playerId);
            
            $info = $db->getFighterById($fighterId);
            $lvl = $db->levelUp($fighterId);
            $this->set('info',$info);
            $this->set('lvl',$lvl);
    

        }

        public function addpoint(){
            
                $playerId = $this->request->session()->read('Auth.User')['id'];
                $db = $this->loadModel('Fighters');
                $fighterId = $db->getFighterId($playerId);
                $data = $this->request->getData();
                if($db->levelUp($fighterId) == 1 ){                
                $db->addPoint($fighterId,$data['upgrade']);
                }
                
                return $this->redirect(
                    ['controller' => 'Fighter', 'action' => 'index']
                );
        }       

    }

