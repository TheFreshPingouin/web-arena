<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;

class ConnectionController extends AppController {
    
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout']);
    }
    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow('index');
        $this->Auth->allow('signin');
        $this->Auth->allow('add');
        $this->Auth->allow('login');
        $this->Auth->allow('pwd');
    }
    
    public function index() {
        
    }

    public function login() {
        if($this->request->is('post')) {
            $user = $this->Auth->identify();
            if($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            
            $this->Flash->error('Bad Login');
        }
    }
    
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }
    
    public function signin()
    {
        
    }
    
    public function pwd() {
        
        $email = $this->request->getData()['email'];
        
        $db = $this->loadModel('Players');
        $password = $db->getPasswordByPlayerEmail($email);
        $this->set('password',$password);
    }
    
    public function fighter() {
        
        $playerId = $this->request->session()->read('Auth.User')['id'];
            
            $db = $this->loadModel('Fighters');
            
            
            $data = $this->request->getData();
            
            
            if($data['fighter_name'] ){
        
                        if( preg_match("/[a-z]/i", $data['fighter_name'])){
            
                            $db->newFighter($playerId,$data['fighter_name']);
                            
                            return $this->redirect(['controller' => 'fighter', 'action' => 'index']);
                        }
                        else{
        
                            pr('<div class="alert alert-warning" role="warning">Your fighter name needs to contain letters</div>');
                        }
                    }
                    else{
                        
                             pr('<div class="alert alert-warning" role="warning">choose a name for your fighter</div>');
                    }
    }
    
     public function add()
    {
        $db = $this->loadModel('Players');
        $user = $this->Players->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Players->patchEntity($user, $this->request->getData());
            if ($this->Players->save($user)) {
                $this->Flash->success(__("L'utilisateur a été sauvegardé."));
                return $this->redirect(['controller' => 'connection', 'action' => 'fighter']);
            }
            $this->Flash->error(__("Impossible d'ajouter l'utilisateur."));
        }
        $this->set('user', $user);
    }
}

