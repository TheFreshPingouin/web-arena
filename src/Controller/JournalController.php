<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\I18n\Time;

class JournalController extends AppController 
{
    
    public function index() 
    {
        $db = $this->loadModel('Fighters');
        $playerId = $this->request->session()->read('Auth.User')['id'];
        $fighterId = $db->getFighterId($playerId);
        $info = $db->getFighterById($fighterId);

        $db = $this->loadModel('Events');
        $events = $db->getRecentEvents();
        
        for ($i = 0; $i < count($events); $i++) {

            $Manhattan = abs($info['coordinate_x']-$events[$i]['coordinate_x']) + abs($info['coordinate_y']-$events[$i]['coordinate_y']);
            if($Manhattan > $info['skill_sight'] ){
                unset($events[$i]);
            }
        }
        $this->set('events',$events);
        
    }
    
}
