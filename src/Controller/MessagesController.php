<?php

namespace App\Controller;
use App\Controller\AppController;

class MessagesController extends AppController {
    
    public function index() {
        // Load models
        $mess = $this->loadModel('Messages');
        $fight = $this->loadModel('Fighters');
        
        // A mettre en données de session
        $userId = $this->request->session()->read('Auth.User')['id'];
        $fighterId = $fight->getFighterId($userId);
        
        // Si index est appelé en ajax, on fait un truc particulier
        if($this->request->is('post')){
            $content = $this->request->getData('content');
            $tofighter = $this->request->getData('tofighter');
            if($tofighter != '' && $content != ''){
                // Send a message
            }
        }
        
        $messList = $mess->getMyMessages($fighterId);
        
        // Refresh all messages for the player
        $this->set($messages, $messList);
    }
}