<?php
namespace App\Model\Table;

use Cake\ORM\Table;

class FightersTable extends Table
{
    
    public function updateFighterById($id, $dep, $traySize){
        $fighter=$this->get($id);
        switch ($dep){
        case 0:
            if($fighter->coordinate_x > 0){
                $fighter->coordinate_x -= 1;
            } else return 0;
            break;
        case 1:
            if($fighter->coordinate_y > 0){
                $fighter->coordinate_y -= 1;
            } else return 0;
            break;
        case 2:
            if($fighter->coordinate_y < $traySize['h'] - 1){
                $fighter->coordinate_y += 1;
            } else return 0;
            break;
        case 3:
            if($fighter->coordinate_x < $traySize['w'] - 1){
                $fighter->coordinate_x += 1;
            } else return 0;
            break;
        default:
            return 0;
        }
        $this->save($fighter);
        return 1;
    }
    
    public function getAllFighters() {
        return $this->find('all')->toArray();
    }
    
    public function attack($enemy, $fi) {
        $random = rand(1, 20);
        if($random > 10 + $enemy->level - $fi->level){
            $enemy->current_health -= $fi->skill_strength;
            $fi->current_health -= $enemy->skill_strength;
            if($fi->current_health < 0){
                $fi->current_health = 0;
            }
            if($enemy->current_health < 0){
                $enemy->current_health = 0;
            }
            $fi->xp += 1;
            
            $this->save($enemy);
            $this->save($fi);
            return true;
        }
        else {
            // The attack didn't work => Nothing changed
            return false;
        }
    }

    public function isAnyFighterOn($fid, $dir) {
        $currentFighter = $this->getFighterById($fid);
        $x1 = $currentFighter['coordinate_x'];
        $y1 = $currentFighter['coordinate_y'];
        $fighters = $this->getAllFighters();
        
        foreach ($fighters as $key => $f) {
            $x2 = $f['coordinate_x'];
            $y2 = $f['coordinate_y'];
            if(($currentFighter['id'] != $f['id']) && $f['current_health'] > 0){
                switch ($dir){
                    case 0:
                        if(($x2 == ($x1 - 1)) && ($y2 == ($y1))){
                            return $f;
                        }
                        break;
                    case 1:
                        if(($x2 == ($x1)) && ($y2 == ($y1 - 1))){
                            return $f;
                        }
                        break;
                    case 2:
                        if(($x2 == ($x1)) && ($y2 == ($y1 + 1))){
                            return $f;
                        }
                        break;
                    case 3:
                        if(($x2 == ($x1 + 1)) && ($y2 == ($y1))){
                            return $f;
                        }
                        break;
                }
            }
        }
        return false;
    }


    public function getFighterById($id){
        $query=$this->find()
                    ->where(['id' => $id]);
        if($query->first() != null){
            return $query->first()->toArray();
        }
        else{
            //debug($query);
            return null;
        }
    }

    //checks if the fighter has passed a level
    public function levelUp($id){

        $query=$this->find()->where(['id' => $id]);
        $query->select(['xp','level']);

        $result = $query->first()->toArray();

        if($result['xp']-(($result['level']-1)*4)>=4){

            return 1;

        }
        else return 0;

    }

    public function addPoint($id,$skill){
        $skillname = 'skill_' . $skill;
        $level = 'level';
        $fighter=$this->get($id);
        if($skill == 'health'){

            $fighter->$skillname=$fighter->$skillname+3;
            $currenthealth = 'current_health';
            $fighter->$currenthealth=$fighter->$skillname;
            
        }
        else{
            $fighter->$skillname=$fighter->$skillname+1;
        }
        $fighter->$level=$fighter->$level+1;
        $this->save($fighter);
      }

      public function newFighter($playerId,$fighterName){

        //on elimine d'abord le précédent fighter potentiellement encore vivant de ce joueur ()
        $fighterToKill = $this->find()
        ->where(['player_id =' => $playerId, 'current_health >' => -1])
        ->first();
        if($fighterToKill){
            
            $fighterToKill->current_health = -1; //exclu de la partie
            $this->save($fighterToKill);

        }

        //creating the character...
        //first, we need to make sure that we find en empty case to spawn our new fighter:




        do{

            $coordX = rand(0,14);
            $coordY = rand(0,9);
            $busyCase = $this->find()
            ->where(['current_health >' => 0,'coordinate_x =' => $coordX,'coordinate_y =' => $coordY])
            ->first();


        } while($busyCase);


        //saving the new player...
        
        $fighter = $this->newEntity();
        
        $fighter->name = $fighterName;
        $fighter->player_id = $playerId;
        $fighter->coordinate_x = $coordX;
        $fighter->coordinate_y = $coordY;
        $fighter->level = 1;
        $fighter->xp = 0;
        $fighter->skill_sight = 2;
        $fighter->skill_strength = 1;
        $fighter->skill_health = 5;
        $fighter->current_health = 5;
        $this->save($fighter);



      }

      public function getFighterId($playerId){

        $fighter = $this->find()
        ->where(['player_id =' => $playerId, 'current_health >' => -1])
        ->first();
        
        if($fighter != null)
            return $fighter->id;
        else
            return false;
      }

}