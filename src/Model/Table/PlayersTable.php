<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class PlayersTable extends Table {
    
    public function test(){
        return $this->find('all')
                ->toArray();
    }

    public function getInfo(){

        return $this->find('all')
        ->toArray();

    }
    
    public function getPasswordByPlayerEmail($email){
        $query=$this->find()
                    ->where(['email' => $email]);
        if($query->first() != null){
            return $query->first()->toArray()['password'];
        }
        else{
            return null;
        }
    }
    
    
}