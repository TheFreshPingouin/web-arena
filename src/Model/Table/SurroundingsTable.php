<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class SurroundingsTable extends Table {
    
    public function getOneSurrounding($i, $j) {
        return $this->select('type')
                ->where([
                    'coordinate_x' => $i,
                    'coordinate_y' => $j
                ]);
    }
    
    public function getSurroundings() {
        return $this->find('all')
                ->toArray();
    }
    
    public function updateSurroundings($traySize, $fighterInfo, $fighters) {
        // Update arena
        $newData = array();
        for ($i = 0; $i < $traySize['w']; $i++) {
            for ($j = 0; $j < $traySize['h']; $j++) {
                // Preparing data for the view
                $access = $i * $traySize['w'] + $j;
                if($fighterInfo['current_health'] <= 0){
                    $newData[$access] = 'X';
                }
                else {
                    $distance = abs($i - $fighterInfo['coordinate_x']) + abs($j - $fighterInfo['coordinate_y']);
                    if($distance <= $fighterInfo['skill_sight']){
                        // Case dans la zone de vision du Joueur
                        if($i == $fighterInfo['coordinate_x'] && $j == $fighterInfo['coordinate_y']){
                            // C'est le joueur
                            $newData[$access] = 'O';
                        }
                        else {
                            $fighterFound = false;
                            foreach ($fighters as $key => $value) {
                                if($i == $value['coordinate_x'] && $j == $value['coordinate_y']){
                                    // There is another fighter !!!!!
                                    if($value['current_health'] > 0) {
                                        $fighterFound = true;
                                        break;
                                    }
                                }
                            }
                            if($fighterFound){
                                $newData[$access] = 'E';
                            }
                            else{
                                // C'est une case visible par le joueur
                                $newData[$access] = '_';
                            }
                        }
                    }
                    else{
                        $newData[$access] = 'X';
                    }
                }
            }
        }
        return $newData;
    }


    public function generateSurroundings($traySize, $fighterInfo, $fighters) {
        // Fetch data from Database
        $data = $this->getSurroundings();
        
        // Deletion of previous data
        if($data == null){
            //$this->deleteAll();
            //$this->find('all')->execute('TRUNCATE TABLE surroundings');
            
            // Delete previous arena
            $this->find('all')->delete()->execute();
            
            // New arena
            for ($i = 0; $i < $traySize['w']; $i++) {
                for ($j = 0; $j < $traySize['h']; $j++) {
                    // Inserting values into the database
                    $this->query()
                            ->insert(['type', 'coordinate_x', 'coordinate_y'])
                            ->values([
                                'type'          => 'XEmpty',
                                'coordinate_x'  => $i,
                                'coordinate_y'  => $j
                            ])
                            ->execute();
                }
            }
        }
        
        // Update the data
        return $this->updateSurroundings($traySize, $fighterInfo, $fighters);
    }
    
}