<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class MessagesTable extends Table {
    
    public function test(){
        return $this->find('all')
                ->toArray();
    }
    
    public function getMyMessages($fid){
        return $this->find('all')
                ->where('fighter_id_from' == $fid OR 'fighter_id' == $fid)
                ->orderDesc('date')
                ->toArray();
    }
    
}