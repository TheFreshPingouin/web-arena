<?php

namespace App\Model\Table;
use Cake\ORM\Table;
use Cake\I18n\Time;


class EventsTable extends Table {
    
    public function setNewEvent($name, $fighterInfo) {
        $e = $this->newEntity();
        
        $e->name = $name;
        $e->date = Time::now();
        $e->coordinate_x = $fighterInfo['coordinate_x'];
        $e->coordinate_y = $fighterInfo['coordinate_y'];
        
        $this->save($e);
    }
    
    public function getRecentEvents(){
        
        $time = Time::now();
        $time->subDays(1);
        
        $events=$this->find()
        ->where(['date >=' => $time])
        ->orderDesc('date');

       /* $events=$this->find('all', array(
            'conditions' => array('date >=' => $time)));*/

        return $events->toArray();
    }
    
}