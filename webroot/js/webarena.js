$(document).ready(function() {
    $('li.active').removeClass('active');
    $('a[href="' + location.pathname + '"]').closest('li').addClass('active');

    
    $('[data-toggle="tooltip"]').tooltip();
    
    reloadMap(-1); // This will run on page load
    //
    // AUTO RELOAD MAKES LITTLE LAG SO IT'S IN COMMENT
    // 
    setInterval(function(){
        reloadMap(-1); // this will run after every 0.3 seconds
    }, 500);
    
    reloadMessages(-1); // This will run on page load
    
    setInterval(function(){
        reloadMessages(-1); // this will run after every 0.3 seconds
    }, 500);
    
    $("#buttonUp").parent().on('click', '#buttonUp', function () {
        return reloadMap(0);
    });
    $("#buttonLeft").parent().on('click', '#buttonLeft', function () {
        return reloadMap(1);
    });
    $("#buttonRight").parent().on('click', '#buttonRight', function () {
        return reloadMap(2);
    });
    $("#buttonDown").parent().on('click', '#buttonDown', function () {
        return reloadMap(3);
    });
    
    $('#myTable').DataTable();
    
    $(document).on('click', ".select", function () {
        console.log("executed function");
        var ichild = $(this).index();
        var iparent = $(this).parent().index();
        var d = $(this).parent();
        var e = d.parent();
        var dep = -1;
        if(d.find(':nth-child('+ ichild +')').hasClass('player')){
            dep = 2;
            console.log("dep = droite");
        }
        if(d.find(':nth-child('+ (ichild+2) +')').hasClass('player')){
            dep = 1;
            console.log("dep = gauche");
        }
        if(e.find(':nth-child('+ iparent +')').find(':nth-child('+ (ichild+1) +')').hasClass('player')){
            dep = 3;
            console.log("dep = bas");
        }
        if(e.find(':nth-child('+ (iparent+2) +')').find(':nth-child('+ (ichild+1) +')').hasClass('player')){
            dep = 0;
            console.log("dep = haut");
        }
        return reloadMap(dep);
    });

});

function reloadMessages(value) {
	var pathname = window.location.pathname;
	var basepath = pathname.substring(0, 10);
	console.log(pathname);
	if(pathname == '/webarena/vision' || pathname == '/webArena/vision'){
	    $.ajax({
	        type: "POST",
	        url: basepath + '/messages',
	        //data: sel.parent().children('input').attr('name') +"="+ sel.parent().children('input').attr('value'),
	        data: "value="+ value,
	        success: function(datar){
	            $("#messenger").html(datar);
	            console.log("reloaded messages");
	        }
	    });
	}
    return false;
}

function reloadMap(dir) {
	var pathname = window.location.pathname;
	//var basepath = pathname.substring(0, 10);
	console.log(pathname);
	if(pathname == '/webarena/vision' || pathname == '/webArena/vision'){
	    $.ajax({
	        type: "POST",
	        url: pathname + '/map',
	        //data: sel.parent().children('input').attr('name') +"="+ sel.parent().children('input').attr('value'),
	        data: "dep="+ dir,
	        success: function(datar){
	            $("#map").html(datar);
	            console.log("moved");
	        }
	    });
	}
    return false;
}

$(document).keydown(function(e){
    // When key pressed
    var dir = -1;
    if(e.which == 38){
        e.preventDefault();
        dir = 0;
    }
    if(e.which == 37){
        e.preventDefault();
        dir = 1;
    }
    if(e.which == 39){
        e.preventDefault();
        dir = 2;
    }
    if(e.which == 40){
        e.preventDefault();
        dir = 3;
    }
    console.log("Key " + e.which);
    reloadMap(dir);
});